var express = require("express");
var http = require("http");
var request = require("request");
var nodemailer = require("nodemailer");
var q = require("q");
var mongoose = require("mongoose");


var app = express();
app.set('port', process.env.PORT || 3013);

var smtpTransport = nodemailer.createTransport("SMTP", {
    host: "mail.snappyapps.com.au",
    secureConnection: false,
    port: 25,
    auth: {
        user: "sales@snappyapps.com.au",
        pass: "ry1an234"
    }
});

mongoose.connect("mongodb://dedicated.snappyapps.com.au/facebookcj");
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.on("open", function () {
    console.log("Database connection successful");
});

var pagePostSchema = mongoose.Schema({
    title: String,
    url: String,
    email: String
});

var PagePost = mongoose.model("PagePost", pagePostSchema);

function downloadReddit(subreddit) {
    var deferred = q.defer();
    request("http://www.reddit.com/" + subreddit + ".json?limit=100", function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var json = JSON.parse(body);
            deferred.resolve(json);
        }
    });
    return deferred.promise;
}

function sendEmail(to, subject, attachmentUrl) {

    var mailOptions = {
        from: "ryan.knell@gmail.com",
        to: to,
        text: "",
        html: "",
        subject: subject,

    }

    if(attachmentUrl !== null){
        mailOptions.attachments = [
            {
                fileName: "post.jpg",
                filePath: attachmentUrl
            }
        ]
    }

    smtpTransport.sendMail(mailOptions, function (err, res) {
        if (err) {
            console.log("Error");
        } else {
            console.log("Message Sent:", res.message);
        }
    });
}

function go() {
    pageSubreddit.forEach(function (ps) {
        downloadReddit(ps.subreddit).then(function (data) {
            console.log("Finished reddit download");
            data.data.children.forEach(function (item) {
                var post = item.data;
                if (post.url.split(".").pop() === "jpg" || ps.isStatus === true) {
                    console.log("Searching the db for " + post.url)
                    PagePost.find({url: post.url, email: ps.postEmail}, function (err, docs) {
                        if (err) {
                            console.log(err);
                        }
                        try{
                            if (docs.length === 0) {
                                var pp = new PagePost({
                                    title: post.title,
                                    url: post.url,
                                    email: ps.postEmail
                                });
                                pp.save();

                                console.log(post.title);
                                if (post.over_18 === false && ps.isStatus !== true) {
                                    sendEmail(ps.postEmail, post.title, post.url);
                                }
                                if (ps.isStatus === true){
                                    sendEmail(ps.postEmail, post.title, null);
                                }
                            } else {
                                console.log(post.url + " already in the database");
                            }
                        } catch (e) {
                            console.log(e);
                        }

                    });

                }

            });
        });
    })


}

var pageSubreddit = [
    {
        //Just Gone Viral
        postEmail: "bubbly141drain@m.facebook.com",
        subreddit: ""
    },
    {
        //Funny Shiz
        postEmail: "kookie728plague@m.facebook.com",
        subreddit: "/r/funny"
    },
    {
        //What what what?
        postEmail: "aline688geegaw@m.facebook.com",
        subreddit: "/r/wtf"
    },
    {
        postEmail: "nates474thad@m.facebook.com",
        subreddit: "/r/funny"
    },
    {
        postEmail:"ribbed847yacht@m.facebook.com",
        subreddit: "/r/funny"
    },
    {
        //Forwards from Grandma
        postEmail:"focal208wailer@m.facebook.com",
        subreddit: "/r/forwardsfromgrandma"
    },
    {
        //Super Cute
        postEmail:"dobra226spotty@m.facebook.com",
        subreddit: "/r/aww"
    },
    {
        //Advice Animals
        postEmail:"dais673lose@m.facebook.com",
        subreddit: "/r/adviceanimals"
    },
    {
        //Funny Shiz
        postEmail: "kookie728plague@m.facebook.com",
        subreddit: "/r/adviceanimals"
    },
    {
        //Shower Thoughts
        postEmail: "labile469slyly@m.facebook.com",
        subreddit: "/r/showerthoughts",
        isStatus: true
    },
    {
        //Food
        postEmail:"foamy716tingle@m.facebook.com",
        subreddit: "/r/food"
    },
    {
        //Photoshop Battles
        postEmail: "bistro720soup@m.facebook.com",
        subreddit: "/r/photoshopbattles"
    },
    {
        //Cringe Pics
        postEmail:"throve752watch@m.facebook.com",
        subreddit: "/r/cringepics"
    },
    {
        //4Chan
        postEmail: "befog883suited@m.facebook.com",
        subreddit: "/r/4chan"
    },
    {
        //Makeup addiction
        postEmail:"leroy959unsnap@m.facebook.com",
        subreddit:"/r/MakeupAddiction"
    },
    {
        //Hubba hubba
        postEmail:"gasket256leanly@m.facebook.com",
        subreddit:"/r/gentlemanboners"
    },
    {
        //Catz
        postEmail:"calk755metis@m.facebook.com",
        subreddit: "/r/cats"
    },
    {
        //Animals Being Jerks
        postEmail: "muses996siva@m.facebook.com",
        subreddit: "/r/AnimalsBeingJerks"
    }
]

go();

setInterval(go, 1000 * 60 * 10);
